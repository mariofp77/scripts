% Program by Mario Fernandez-Pendas
%
% This program generates the values of the function
% rho for the velocity Verlet integrator for an
% interval given as an input.
%
% One file is printed: the output of the function.

function []=vv(hmin,hmax)

h=linspace(hmin,hmax,10000);
n=length(h);
b=0.25
f1 = fopen('vv.dat','w');

for i=1:n
  % Calculate the function
  rho(i) = h(i)^4*(2*b^2*(1/2-b)*h(i)^2+4*b^2-6*b+1)^2/(8*(2-b*h(i)^2)*(2-(1/2-b)*h(i)^2)*(1-b*(1/2-b)*h(i)^2));
  % Write in a file
  fprintf(f1,'%d\t%d\n',h(i),rho(i));
endfor
fclose(f1);

% Print
plot(h,rho);
axis([hmin hmax 0 0.001]);
