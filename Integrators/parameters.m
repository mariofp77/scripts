% Program by Mario Fernandez-Pendas
%
% This program maximizes the function rho for two-stage
% integrators, given an interval of time steps with its
% maximum as as an input. Another input is the type of
% method: 'GSHMC' or 'HMC', where these values decide
% if modified Hamiltonians are being considered or not,
% respectively.
%
% The output is the value b that minimizes the maximum
% of rho in the desired interval defining the desired
% integrator.

function [b] = parameters(method, dt_scaled)

dt_trial = 0;
dt2 = 0;
dt4 = 0;
da2   = 0.25; % VV parameter
daux = 0;
drhoAux = 0;
drho2 = 0;

% Maximun value of rho for two VV steps concatenated
while (dt_trial < dt_scaled)
{
   dt_trial = dt_trial + 0.01;
   dt2 = sqr(dt_trial);
   dt4 = sqr(dt2);
   daux = sqr(2*sqr(da2)*(0.5-da2)*dt2+4*sqr(da2)-6*da2+1)*1e3;
   daux = daux/(2-da2*dt2);
   daux = daux/(2-(0.5-da2)*dt2);
   daux = daux/(1-da2*(0.5-da2)*dt2);
   drhoAux = dt4*daux*0.125;
   if (drhoAux > drho2)
      drho2 = drhoAux;
}
% Maximun value of rho for two VV steps concatenated

dt_trial = -0.01;

da_opt = da2;
da1 = 0.193183; % ME parameter

while (da1 <= da2)
{
   da1 += 1e-6; % step for the integrator parameter search
   while (dt_trial < dt_scaled)
   {
      dt_trial = dt_trial + 0.01;
      dt2 = sqr(dt_trial);
      dt4 = sqr(dt2);
      daux = sqr(2*sqr(da1)*(0.5-da1)*dt2+4*sqr(da1)-6*da1+1)*1e3;
      daux = daux/(2-da1*dt2);
      daux = daux/(2-(0.5-da1)*dt2);
      daux = daux/(1-da1*(0.5-da1)*dt2);
      drhoAux = dt4*daux*0.125;
      if (drhoAux > drho1)
         drho1 = drhoAux;
   }
   dt_trial = -0.01;
   if (drho2 > drho1)
   {
      drho2 = drho1;
      da_opt = da1;
   }
   drho1 = 0;
}
ir->dIntA = da_opt;
printf("The optimal parameter a is %f\n",da_opt);

