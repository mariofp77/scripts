% Program by Mario Fernandez-Pendas
%
% This program generates the values of the function
% rho, for modified Hamiltonians, calculated for three
% integrators specified by their parameters given as
% inputs. Among these values, the function also selects
% the minimum for every time step.
% The size of the grid of time steps is another input
% parameter.
%
% Three files are printed: one for each of the three
% integrators for the time steps in which it gives the
% minimum value.

function [rho,minrho]=combinedmodifiedrhos(b1,b2,b3,n)

h=linspace(0,4,n); % the size of the mesh is an input

% initialize the output
rho=zeros(n,3);
minrho=zeros(n,1);

f1 = fopen('method1.dat','w');
f2 = fopen('method2.dat','w');
f3 = fopen('method3.dat','w');

for (j=1:3)
  % select the three methods
  if (j==1)
    b=b1;
  end
  if (j==2)
    b=b2;
  end
  if (j==3)
    b=b3;
  end
  
  % do the for over the different time steps
  for (I=1:n)
    h2 = (h(I))^2;
    h8 = (h2)^4;
    aux = b*(1 + 4*b*(3*b - 2))*h2;
    aux = b * (12 + 4*b*(6*b-5) + aux);
    aux = h8 * (aux - 2)^2 * 1000;
    aux = aux/(2-b*h2)/4;
    aux1 = (2*b - 1)*h2;
    aux = aux / (4 + aux1);
    aux = aux / (2 + b*aux1);
    aux = aux / (12 + (6*b - 1)*h2);
    rhoAux = aux / (6 + (1+6*(b-1)*b)*h2);
    rho(I,j) = rhoAux/1000;
  endfor
endfor

% select the minimum per row among the matrix of maxima
% maintain the label of the method selected
for (i=1:n)
  [minrho(i), indx] = min([abs(rho(i,1)),abs(rho(i,2)),abs(rho(i,3))]);
  
  if (indx == 1)
    fprintf(f1,'%d\t%d\n',h(i),minrho(i));
  end
  if (indx == 2)
    fprintf(f2,'%d\t%d\n',h(i),minrho(i));
  end
  if (indx == 3)
    fprintf(f3,'%d\t%d\n',h(i),minrho(i));
  end
end

fclose(f1);
fclose(f2);
fclose(f3);
