% Program by Mario Fernandez-Pendas
%
% This program maximizes the function rho for any
% integrator, given its parameter as an input.
% Another input is the type of method:
% 'GSHMC' or 'HMC', where these values decide if
% modified Hamiltonians are being considered or not,
% respectively.
%
% The output is the value of the maximum and the
% time step where it is reached.

function [maxrho, h] = max_rho(method, b)

h_incr = 0.001;
rho = -10; 

h_trial = 0;
i = 1;

switch method
    case 'GSHMC'
        while (h_trial < 4) 
            h_trial = h_trial + h_incr;
            h2 = (h_trial)^2;
            h8 = (h2)^4;    
            aux = b*(1 + 4*b*(3*b - 2))*h2;
            aux = b * (12 + 4*b*(6*b-5) + aux);
            aux = h8 * (aux - 2)^2 * 1000;   
            aux = aux/(2-b*h2)/4;
            aux1 = (2*b - 1)*h2;
            aux = aux / (4 + aux1);
            aux = aux / (2 + b*aux1);
            aux = aux / (12 + (6*b - 1)*h2);
            rhoAux = aux / (6 + (1+6*(b-1)*b)*h2);
            if (rhoAux > rho)
                rho = rhoAux;
            end
            h(i) = h_trial;
            maxrho(i) = rho/1000;
            i = i + 1;
        end

    case 'HMC'
        while (h_trial < 4) 
            h_trial = h_trial + h_incr;
            h2 = (h_trial)^2;
            h4 = (h2)^2; 
            aux1 = (1-2*b)*h2;
            aux = 6 - b*(4 + aux1);
            aux = 1 - b*aux;
            aux = h4*aux^2 * 1000;
            aux = aux/(2-b*h2)/2;
            aux = aux/(4 - aux1);
            rhoAux = aux/(2 - b*aux1);
            if (rhoAux > rho)
                rho = rhoAux;
            end
            h(i) = h_trial;
            maxrho(i) = rho/1000;
            i = i + 1;
        end
end
    
        
end
