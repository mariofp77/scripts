#!/usr/bin/python3

# Program by Mario Fernandez-Pendas
#
# This program computes a new definition of scalings using the averages
# of the acceptance rates and the time steps used for obtaining them.
# It needs the acceptance rates file ('input1') and the combinations file
# ('input2').
# 
# Usage: average_scaling.py input1 input2

import re
import numpy as np

from optparse import OptionParser

# Some ANSI colors
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'

parser = OptionParser()
parser.add_option("-v","--verbose",dest="verbose",help="Print extra info",action="store_true")
(options,args) = parser.parse_args()

try:
    arinputfile = open(args[0],"r")
    dtinputfile = open(args[1],"r")
except IOError:
    sys.stderr.write(FAIL)
    sys.stderr.write("There was a problem opening the file. Does it exist at all?")
    sys.stderr.write(ENDC+"\n")
    sys.exit(1)

filelines_ar = arinputfile.readlines()
number_of_lines = len(filelines_ar)

div = re.compile("D1000_div")
norm1 = re.compile("D1000_norm_00001_000001")
norm2 = re.compile("D1000_norm_1_01_990_norm_4000_40_10")
unif = re.compile("D1000_unif_10_50")
german = re.compile("german")

# Computation of the average acceptance rate
total_sum = 0

for line in filelines_ar:
    ar = float(line.split()[1])
    total_sum = total_sum + ar

average_ar = total_sum/number_of_lines

filelines_dt = dtinputfile.readlines()
number_of_lines = len(filelines_dt)

# Computation of the average time step
total_sum = 0
timestep = re.compile(" stepsize ")
for line in filelines_dt:
    # Dimension of the system and maximum frequency
    if (div.search(line) and total_sum == 0):
        D = 1000
        w = 62.97913
    elif (norm1.search(line) and total_sum == 0):
        D = 1000
        w = 0.01146148
    elif (norm2.search(line) and total_sum == 0):
        D = 1000
        w = 63.78791
    elif (unif.search(line) and total_sum == 0):
        D = 1000
        w = 7.068875
    elif (german.search(line) and total_sum == 0):
        D = 25
        w = 13.09677
    dt = float(line.split()[23])
    total_sum = total_sum + dt
    
average_dt = total_sum/number_of_lines

# Calculation of the scaling
root = np.power(32*average_ar/D,1/6)
S = root/(average_dt*w)
print(S)
