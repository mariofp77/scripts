#!/usr/bin/python2

# Program by Mario Fernandez-Pendas
#
# This program, given several batches of ordered eigenvalues in
# an output file from HAICS, places them in different files in
# order to plot or analyse them later.
# 
# Usage: blrEigenvalues.py input (int)dimensions

import commands
import os
import sys
import math
import re

from optparse import OptionParser

# Some ANSI colors
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'

parser = OptionParser()
parser.add_option("-v","--verbose",dest="verbose",help="Print extra info",action="store_true")
(options,args) = parser.parse_args()

try:
	inputfile = file(args[0],"r")
except IOError:
	sys.stderr.write(FAIL)
	sys.stderr.write("There was a problem opening the file. Does it exist at all?")
	sys.stderr.write(ENDC+"\n")
	sys.exit(1)

filelines = inputfile.readlines()
number_of_lines = len(filelines)

# Number of variables used
dim = int(args[1])
# Number of sequences
seq = number_of_lines/dim

if inputfile != None:
    for j in range(seq):
        resfile = open("output" + str(j+1),"w")
        i = 0
        for line in filelines:
            if (i < (j+1)*dim and i >= j*dim):
                aux = float(line)
                resfile.write("%5.8f \n" % aux)
            i+=1
        resfile.close()
