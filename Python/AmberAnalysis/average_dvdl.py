#!/usr/bin/python3

# Program by Mario Fernandez-Pendas
#
# This program computes an average of each dvdl file
# (corresponding to the different lambda windows) and 
# gives an output file with the lambdas in one column
# and the averages in another one.
# 
# Usage: average_dvdl.py

import numpy as np
import os
import fnmatch

filenames = []
lambdas   = []

# We select all the dvdl files and we store their
# names ordered
for filename in os.listdir('.'):
    if fnmatch.fnmatch(filename,'dvdl_*.dat'):
        filenames.append(filename)
filenames.sort()

# Number of files/lambda windows
N = len(filenames)

# We create a list of the lambdas used
for i in range(N):
    lambdas.append(filenames[i].replace('dvdl_',''))
    lambdas[i] = lambdas[i].replace('.dat','')

positions = []

# We read all the files
for i in range(N):
    with open(filenames[i]) as file:
        lines = (line for line in file)
        positions.append(np.loadtxt(lines))

# We compute the average for each file/lambda window
dim = len(positions[0]) # Number of points we need for the average
suma = np.zeros(N)
for line in range(dim):
    for i in range(N):
        suma[i] = suma[i] + positions[i][line,1]
aver = suma/dim

if os.path.exists('dvdl.dat'):
    os.remove('dvdl.dat')
with open("dvdl.dat", "w") as f:
    for i in range(N):
        f.write("%s %s\n" %(lambdas[i], aver[i]))
