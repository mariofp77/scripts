#!/usr/bin/python3

# Program by Mario Fernandez-Pendas
#
# This program computes the scaling for a set of time steps
# using the acceptance rates and an average time step.
# A file called 'scalings.txt' is required. Its first
# column are the time steps (with units of measure)
# and the second are the S1's obtained before.
#
# Version 0.24.2 of pandas is required
# 
# Usage: scaling_ad.py

import numpy as np
import pandas as pd

data = pd.read_csv('scalings.txt', sep=" ", header=None)
data.columns = ["a", "b"]

size=len(data)

# Computation of the average S1
d=data['a']*data['b'] # Each cas has to be multiplied by the time step since later we will use the average
d=np.array(d)
suma=np.sum(d)
average=suma/size

# Computation of the average time step
dt = np.array(data['a'])
sum_dt = np.sum(dt)
av_dt = sum_dt/size

# Computation of the scaling
S = average/av_dt
print(S)
