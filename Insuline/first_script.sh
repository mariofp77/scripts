#!/bin/bash

#SBATCH --partition=batch
#SBATCH --job-name=martini
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --time=2-00:00:00

tpr="topol.tpr"
np=1
ntomp=1
eu="1.2"
tpr="2beg_nmol20_elnedyn22_eu${eu}_random_solv_eq"

mpirun -np $np /opt/local/gromacs/2021.1/bin/mdrun_mpi -ntomp $ntomp -s $tpr -deffnm $tpr
