set term pdfcairo color solid lw 2 size 4in,3in
set out "Lo-states.pdf"

########################################################################
# Data from Lo & Palmer:

$Liquid << EOD
# Table I
# T*      mu*      rho*(GCE)   P*(GCE)     rho*(EOS)   P*(EOS)
#----------------------------------------------------------------
0.769   22.999       0.785      0.296        0.780     0.324
#0.769   22.868       0.794      0.384        0.790     0.427
0.769   22.727       0.804      0.521        0.800     0.539
#0.769   20.816       0.895      2.190        0.890     2.163
1.0     22.852       0.640      0.209        0.650     0.226
#1.0     22.403       0.707      0.558        0.700     0.530
1.0     21.757       0.754      1.001        0.750     0.999
#1.0     21.258       0.783      1.366        0.780     1.381
#1.0     20.870       0.804      0.697        0.800     1.687
EOD

$Gas << EOD
# Table II in Lo_1995_JCP_102_925:
#  T*      mu*      rho*(GCE)   P*(GCE)     rho*(EOS)   P*(EOS)
#----------------------------------------------------------------
0.769   24.127       0.0050    0.00354       0.0050    0.00371
#0.769   23.797       0.0081    0.00567       0.0080    0.00582
0.769   23.646       0.0099    0.00696       0.0100    0.00717
#0.769   23.583       0.0108    0.00758       0.0110    0.00783
1.0     23.200       0.0699    0.05020       0.0700    0.05152
#1.0     23.162       0.0808    0.05484       0.0760    0.05434
1.0     23.150       0.0844    0.05600       0.0780    0.05523
#1.0     23.139       0.0896    0.05792       0.0800    0.05609
EOD

########################################################################
# Data from http://www.sklogwiki.org/SklogWiki/index.php/Lennard-Jones_model
# This refers to the non-truncated potential, assuming that the
# necessary corrections (using a perturbation scheme) were done after
# the simulations.

$Triple << EOD
#  T*     rho*  
#---------------------------
 0.694    0.84     # liquid
 0.694    0.96     # solid
EOD
Ttp = 0.694

#$Critical << EOD
#  T*      rho*       P*
#---------------------------
# 1.326    0.316    0.1279
#EOD

# van der Hoef melting lines (not very good agreement with $Triple):
vdH_solid(T) = (T<Ttp ? NaN : (1/T)**(-0.25) * \
  ( 0.92302 - 0.09218*(1/T) + 0.62381*(1/T)**2 - 0.82672*(1/T)**3 \
    + 0.49124*(1/T)**4 - 0.10847*(1/T)**5 ) )
vdH_liquid(T) = (T<Ttp ? NaN : (1/T)**(-0.25) * \
  ( 0.91070 - 0.25124*(1/T) + 0.85861*(1/T)**2 - 1.08918*(1/T)**3 \
    + 0.63932*(1/T)**4 - 0.14433*(1/T)**5 ) )

# Mastny and de Pablo melting lines (seem to be better):
MdP_solid(T) = (T<Ttp ? NaN : (1/T)**(-0.25) * \
  ( 0.908629 - 0.041510*(1/T) + 0.514632*(1/T)**2 - 0.708590*(1/T)**3 \
    + 0.428351*(1/T)**4 - 0.095229*(1/T)**5 ) )
MdP_liquid(T) = (T<Ttp ? NaN : (1/T)**(-0.25) * \
  ( 0.90735 - 0.27120*(1/T) + 0.91784*(1/T)**2 - 1.16270*(1/T)**3 \
    + 0.68012*(1/T)**4 - 0.15284*(1/T)**5 ) )

########################################################################
# Make LJ rho-T phase diagram with: (1) Lo & Palmer data (Tables I and
# II); (2) several data from www.sklogwiki.org; (3) diagram from Lin
# et al. (Figure 3, extracted with pdfimages) used as background
# (through x2y2).

rhomin=0 ; rhomax=1.2 ; Tmin=0.6 ; Tmax=1.8

# Make melting datablocks:
set samples 1000
set table $Melting_S   # T, rho_solid(T)
plot [Tmin:Tmax] [rhomin:rhomax] MdP_solid(x)
unset table
set table $Melting_L   # T, rho_liquid(T)
plot [Tmin:Tmax] [rhomin:rhomax] MdP_liquid(x)
unset table

# Try {x,y}{min,max} values until "Lin-fig3.png" is well-fit to the plot axes.
xmin=98.0 ; xmax=960.0 ; ymin=99.0 ; ymax=690.0

set x2range [xmin:xmax]; unset x2tics
set y2range [ymin:ymax]; unset y2tics
set xlabel "{/Symbol r}*" ; set xrange [rhomin:rhomax]
set ylabel "T*"; set yrange [Tmin:Tmax]
set label 1 "First max" at 1.0,1.0 right 
set xtics out mirror; set ytics out mirror
set key left opaque box width -3.5
set label "LIQUID" at  0.75,1.05 front tc ls 0
set label "SOLID" at  1.05,1.05 front tc ls 0
set label "GAS" at  0.02,1.25 front tc ls 0
set label "critical point" at  0.2,1.35 front tc ls 0
set label "SUPER CRITICAL FLUID" at  0.4,1.6 front tc ls 0

plot "Lin-fig3.png" binary filetype=png axes x2y2 notit with rgbimage, \
     $Liquid u ($5):($1) t "L\\&P liq" w p pt 6 lc 1, \
     $Gas u 5:1 t "L\\&P gas" w p pt 6 lc 2, \
     Ttp notit w l lc 4, $Triple u 2:1 t "triple point" w lp pt 6 lc 4, \
     $Melting_S u 2:1 tit "melting" w l lc 6, $Melting_L u 2:1 notit w l lc 6

set key Left opaque nobox width -3.5
plot "Lin-fig3.png" binary filetype=png axes x2y2 notit with rgbimage, \
     $Liquid u ($5):($1) t "Liquid" w p pt 6 lc 1, \
     $Gas u 5:1 t "Gas" w p pt 6 lc 2

