#!/bin/bash

input=$1
nodes=$2
procs=$3
memry=$4
ttime=$5

cores=$(($nodes*$procs))
tcput=$(($cores*$ttime))

name=${input%.*}
cat > ${name}.pbs <<-***
#!/bin/bash
#PBS -q qchem
#PBS -l nodes=$2:ppn=$3
#PBS -l ncpus=$cores
#PBS -l cput=$tcput:00:00
#PBS -l mem=$4gb
#PBS -l walltime=$5:00:00
#PBS -N $input
#PBS -m ea    

export SCRATCH_DIR=/scratch/$USER/wrktmp/\$PBS_JOBID
mkdir -p \$SCRATCH_DIR
cd \$PBS_O_WORKDIR
cp $input \$SCRATCH_DIR
if [ -d "${name}_atomicfiles" ]; then
 cp -r ${name}_atomicfiles \$SCRATCH_DIR
fi
cd \$SCRATCH_DIR

module load AIMAll/17.11.14B

export NPROCS=\`wc -l < \$PBS_NODEFILE\`

time aimqb.ish -nogui -nproc=$3 -naat=$3 $input

cp -r * \$PBS_O_WORKDIR
rm -rf \$SCRATCH_DIR

***
qsub  ${name}.pbs
