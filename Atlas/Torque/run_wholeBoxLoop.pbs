#!/bin/bash
#PBS -q qchem
#PBS -l nodes=1:ppn=24
#PBS -l mem=1gb
#PBS -l cput=17280:00:00
#PBS -N production

export SCRATCH_DIR=/scratch/$USER/wrktmp/$PBS_JOBID
mkdir -p $SCRATCH_DIR
cd $PBS_O_WORKDIR
cp -r data $SCRATCH_DIR
cp -r amber* $SCRATCH_DIR
cp -r *.mdp $SCRATCH_DIR
cp -r *.top $SCRATCH_DIR
cp -r *.itp $SCRATCH_DIR
cp -r *.sh $SCRATCH_DIR
cd $SCRATCH_DIR

module load GROMACS

export NPROCS=`wc -l < $PBS_NODEFILE`

np=1
ntomp=24

wat="tip4pd"
ff="ff99sbd"
nmol=2
for proot in "3q2x" #"2ona" "2kib" "6bwz" "6bzp" "6bzm" "6bxx" "6bxv"
do
	out="data/${proot}_${ff}_${wat}"
	top="${proot}_${ff}_${wat}"
	itp="${proot}_${ff}_${wat}.itp"

	for box in "2.7" "3" "3.5" "4" "4.5"
	do
		inp="data/${proot}_${ff}_${wat}"
		nmol=2
		out="data/${proot}${nmol}_${ff}_${wat}_box${box}"
		nmo=$(expr $nmol - 1)
		gmx insert-molecules -f $inp -ci $inp -nmol $nmo -box $box -o $out
	
		topn="${proot}${nmol}_${ff}_${wat}_box${box}.top"
		nlines=`wc -l ${top}.top | awk '{print $1}'`
		echo $nlines
	 	awk -v nmol=$nmol -v nlines=$nlines -v itp=$itp '\
			/posre.itp/ {print "#include \""itp"\""; next} \
			NR==nlines {print "Protein_chain_A    ", nmol; next}\
			{print}' "${top}.top" > $topn
	
		inp=$out
		out="data/${proot}${nmol}_${ff}_${wat}_box${box}_solv"
		gmx solvate -p $topn -cp $inp -cs amber99sb-disp.ff/tip4p-d.gro -o $out
	
		mdp="mini.mdp"
		inp=$out
		out="data/${proot}${nmol}_${ff}_${wat}_box${box}_ions"
		gmx grompp -f $mdp -c $inp -p $topn -o $out -maxwarn 1
	
		gmx genion -s $out -o $out -p $topn -pname NA -nname CL -neutral <<EOF
	13
EOF
	
		mdp="mini.mdp"
		inp=$out
		out="data/${proot}${nmol}_${ff}_${wat}_box${box}_mini"
		gmx grompp -p $topn -c $inp -f $mdp -o $out -pp 
	
		inp=$out
		out=$inp
		gmx mdrun -ntmpi $np -ntomp $ntomp -v -s $inp -deffnm $out

		# We plot the potential energy to see the convergence of the minimization
		plotmini_i="data/${proot}${nmol}_${ff}_${wat}_box${box}_mini.edr"
		plotmini_o="data/${proot}${nmol}_${ff}_${wat}_box${box}_mini_pot.xvg"
		echo 10 | gmx energy -f $plotmini_i -o $plotmini_o
	
		mdp="nvt_posre.mdp"
		inp=$out
		out="data/${proot}${nmol}_${ff}_${wat}_box${box}_nvt_posre"
		gmx grompp -f $mdp -c ${inp} -p $topn -r $inp -o $out
	
		inp=$out
		out=$inp
		gmx mdrun -ntmpi $np -ntomp $ntomp -v -s $inp -deffnm $out

		# We plot the Hamiltonian, the temperature and the conserved energy to see the
		# behaviour of the thermostat after the position restraints
		plotposre_i="data/${proot}${nmol}_${ff}_${wat}_box${box}_nvt_posre.edr"
		plotposre_o_h="data/${proot}${nmol}_${ff}_${wat}_box${box}_nvt_posre_ham.xvg"
		plotposre_o_c="data/${proot}${nmol}_${ff}_${wat}_box${box}_nvt_posre_con.xvg"
		plotposre_o_t="data/${proot}${nmol}_${ff}_${wat}_box${box}_nvt_posre_tem.xvg"
		echo 13 | gmx energy -f $plotposre_i -o $plotposre_o_h
		echo 14 | gmx energy -f $plotposre_i -o $plotposre_o_c
		echo 15 | gmx energy -f $plotposre_i -o $plotposre_o_t
		
		mdp="npt.mdp"
		inp=$out
		out="data/${proot}${nmol}_${ff}_${wat}_box${box}_npt"
		gmx grompp -f $mdp -c $inp -p $topn -r $inp -o $out -t ${inp}.cpt
	
		inp=$out
		out=$inp
		gmx mdrun -ntmpi $np -ntomp $ntomp -s $inp -deffnm $out
		# We plot the pressure and the density to see the behaviour of the barostat
		plotnpt_i="data/${proot}${nmol}_${ff}_${wat}_box${box}_npt.edr"
		plotnpt_o_p="data/${proot}${nmol}_${ff}_${wat}_box${box}_npt_pres.xvg"
		plotnpt_o_d="data/${proot}${nmol}_${ff}_${wat}_box${box}_npt_den.xvg"
		echo 15 | gmx energy -f $plotnpt_i -o $plotnpt_o_p
		echo 21 | gmx energy -f $plotnpt_i -o $plotnpt_o_d
	
		mdp="sd_nvt.mdp"
		inp=$out
		out="data/${proot}${nmol}_${ff}_${wat}_box${box}_nvt"
		gmx grompp -f $mdp -c $inp -p $topn -r $inp -o $out -t ${inp}.cpt -maxwarn 1
		
		inp=$out
		out=$inp
		gmx mdrun -ntmpi $np -ntomp $ntomp -s $inp -deffnm $out
	done
done
rm \#* data/\#*

export RESULTS_DIR=$PBS_O_WORKDIR/RESULTS_${PBS_JOBID}
mkdir -p $RESULTS_DIR
cp -r * $RESULTS_DIR
rm -rf $SCRATCH_DIR
