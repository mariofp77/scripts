#!/bin/bash
#SBATCH --partition=long
#SBATCH --job-name=prod_big_c36m
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=1
#SBATCH --gres=gpu:p40:1
#SBATCH --time=2-00:00:00
#SBATCH --mem=1G

module load GROMACS/2020-fosscuda-2019b

CDIR=`pwd`

export SCRATCH_DIR=/scratch/$USER/wrktmp/$SLURM_JOBID
mkdir -p $SCRATCH_DIR
cp -r data_bigSystem $SCRATCH_DIR
cp -r charmm* $SCRATCH_DIR
cp -r *.mdp $SCRATCH_DIR
cp -r *.top $SCRATCH_DIR
cp -r *.itp $SCRATCH_DIR
cp -r *.sh $SCRATCH_DIR

cd $SCRATCH_DIR

np=8
ntomp=1

wat="tip3p"
ff="ffc36m"
for proot in "3q2x"
do
	for box in "10"
	do
		peps=8
		top="${proot}${peps}_${ff}_${wat}"
		itp="${proot}_${ff}_${wat}.itp"
		inp="data_bigSystem/${proot}${peps}_${ff}_${wat}"
		nmol=1
		out="data_bigSystem/${proot}${peps}_${ff}_${wat}_box${box}"
		gmx insert-molecules -f $inp -ci $inp -nmol $nmol -try 0 -box $box -o $out
		gmx editconf -f $out -c -o $out
	
		topn="${proot}${peps}_${ff}_${wat}_box${box}.top"
		nlines=`wc -l ${top}.top | awk '{print $1}'`
		echo $nlines
	 	awk -v nmol=$nmol -v nlines=$nlines -v itp=$itp '\
			/posre.itp/ {print "#include \""itp"\""; next} \
			NR==nlines {print "Protein_chain_A    ", nmol; next}\
			{print}' "${top}.top" > $topn
	
		inp=$out
		out="data_bigSystem/${proot}${peps}_${ff}_${wat}_box${box}_solv"
		gmx solvate -p $topn -cp $inp -o $out
	
		mdp="mini_bigSystem_c36m.mdp"
		inp=$out
		out="data_bigSystem/${proot}${peps}_${ff}_${wat}_box${box}_ions"
		gmx grompp -f $mdp -c $inp -p $topn -o $out -maxwarn 1
	
		gmx genion -s $out -o $out -p $topn -pname NA -nname CL -neutral <<EOF
	13
EOF
	
		mdp="mini_bigSystem_c36m.mdp"
		inp=$out
		out="data_bigSystem/${proot}${peps}_${ff}_${wat}_box${box}_mini"
		gmx grompp -p $topn -c $inp -f $mdp -o $out -pp 
	
		inp=$out
		out=$inp
		mpirun --map-by ppr:4:node -np $np gmx_mpi mdrun -ntomp $ntomp -v -s $inp -deffnm $out

		# We plot the potential energy to see the convergence of the minimization
		plotmini_i="data_bigSystem/${proot}${peps}_${ff}_${wat}_box${box}_mini.edr"
		plotmini_o="data_bigSystem/${proot}${peps}_${ff}_${wat}_box${box}_mini_pot.xvg"
		echo 11 | gmx energy -f $plotmini_i -o $plotmini_o
	
		mdp="nvt_posre_c36m.mdp"
		inp=$out
		out="data_bigSystem/${proot}${peps}_${ff}_${wat}_box${box}_nvt_posre"
		gmx grompp -f $mdp -c ${inp} -p $topn -r $inp -o $out
	
		inp=$out
		out=$inp
		mpirun --map-by ppr:4:node -np $np gmx_mpi mdrun -ntomp $ntomp -v -s $inp -deffnm $out

		# We plot the Hamiltonian and the temperature to see the behaviour of the
		# thermostat after the position restraints
		plotposre_i="data_bigSystem/${proot}${peps}_${ff}_${wat}_box${box}_nvt_posre.edr"
		plotposre_o_h="data_bigSystem/${proot}${peps}_${ff}_${wat}_box${box}_nvt_posre_ham.xvg"
		plotposre_o_t="data_bigSystem/${proot}${peps}_${ff}_${wat}_box${box}_nvt_posre_tem.xvg"
		echo 14 | gmx energy -f $plotposre_i -o $plotposre_o_h
		echo 16 | gmx energy -f $plotposre_i -o $plotposre_o_t
		
		mdp="npt_c36m.mdp"
		inp=$out
		out="data_bigSystem/${proot}${peps}_${ff}_${wat}_box${box}_npt"
		gmx grompp -f $mdp -c $inp -p $topn -r $inp -o $out -t ${inp}.cpt
	
		inp=$out
		out=$inp
		mpirun --map-by ppr:4:node -np $np gmx_mpi mdrun -ntomp $ntomp -v -s $inp -deffnm $out
		# We plot the pressure and the density to see the behaviour of the barostat
		plotnpt_i="data_bigSystem/${proot}${peps}_${ff}_${wat}_box${box}_npt.edr"
		plotnpt_o_p="data_bigSystem/${proot}${peps}_${ff}_${wat}_box${box}_npt_pres.xvg"
		plotnpt_o_d="data_bigSystem/${proot}${peps}_${ff}_${wat}_box${box}_npt_den.xvg"
		echo 16 | gmx energy -f $plotnpt_i -o $plotnpt_o_p
		echo 22 | gmx energy -f $plotnpt_i -o $plotnpt_o_d
	
		mdp="sd_nvt_bigSystem_c36m.mdp"
		inp=$out
		out="data_bigSystem/${proot}${peps}_${ff}_${wat}_box${box}_nvt"
		gmx grompp -f $mdp -c $inp -p $topn -r $inp -o $out -t ${inp}.cpt -maxwarn 1
		
		inp=$out
		out=$inp
		mpirun --map-by ppr:4:node -np $np gmx_mpi mdrun -ntomp $ntomp -v -s $inp -deffnm $out
	done
done
rm \#* data_bigSystem/\#*

mkdir -p $CDIR/$SLURM_JOB_ID
cp -r * $CDIR/$SLURM_JOB_ID
rm -fr $SCRATCH_DIR
