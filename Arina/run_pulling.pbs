#!/bin/bash
#PBS -l nodes=1:ppn=12
#PBS -l mem=1gb
#PBS -l cput=25920:00:00
#PBS -N pulling

scrt=/scratch/$USER/$PBS_JOBID
mkdir $scrt
cp -r $PBS_O_WORKDIR/* $scrt
cd $scrt

source /software/gromacs/bin/GMXRC.bash

export NPROCS=`wc -l < $PBS_NODEFILE`

np=1
ntomp=24

proot="1p5u"
shape="Cut"
wat="tip3p"
ff="ff36"

top="${proot}${shape}_${ff}_${wat}_initial"

topn="${proot}${shape}_${ff}_${wat}.top"
nlines=`wc -l ${top}.top | awk '{print $1}'`
echo $nlines
awk -v nmol=$nmol -v nlines=$nlines -v itp=$itp '\
        /posre.itp/ {print "#include \""itp"\""; next} \
        {print}' "${top}.top" > $topn

inp="data/${proot}${shape}_${ff}_${wat}_initial"
out="data/${proot}${shape}_${ff}_${wat}"
gmx editconf -f $inp -c -box 40 6 6 -o $out

inp=$out
out="data/${proot}${shape}_${ff}_${wat}_solv"
gmx solvate -p $topn -cp $inp -o $out

mdp="mini.mdp"
inp=$out
out="data/${proot}${shape}_${ff}_${wat}_ions"
gmx grompp -f $mdp -c $inp -p $topn -o $out -maxwarn 1

gmx genion -s $out -o $out -p $topn -pname NA -nname CL -neutral <<EOF
13
EOF

inp=$out
out="data/${proot}${shape}_${ff}_${wat}_mini"
gmx grompp -p $topn -c $inp -f $mdp -o $out

inp=$out
out=$inp
gmx mdrun -ntmpi $np -ntomp $ntomp -s $inp -deffnm $out

# We plot the potential energy to see the convergence of the
# minimization
plotmini_i="data/${proot}${shape}_${ff}_${wat}_mini.edr"
plotmini_o="data/${proot}${shape}_${ff}_${wat}_mini_pot.xvg"
echo 10 | gmx energy -f $plotmini_i -o $plotmini_o

mdp="nvt_posre.mdp"
inp=$out
out="data/${proot}${shape}_${ff}_${wat}_nvt_posre"
gmx grompp -f $mdp -c ${inp} -p $topn -r $inp -o $out

inp=$out
out=$inp
gmx mdrun -ntmpi $np -ntomp $ntomp -s $inp -deffnm $out

# We plot the Hamiltonian, the temperature and the conserved energy to
# see the
# behaviour of the thermostat after the position restraints
plotposre_i="data/${proot}${shape}_${ff}_${wat}_nvt_posre.edr"
plotposre_o_h="data/${proot}${shape}_${ff}_${wat}_nvt_posre_ham.xvg"
plotposre_o_c="data/${proot}${shape}_${ff}_${wat}_nvt_posre_con.xvg"
plotposre_o_t="data/${proot}${shape}_${ff}_${wat}_nvt_posre_tem.xvg"
echo 14 | gmx energy -f $plotposre_i -o $plotposre_o_h
echo 15 | gmx energy -f $plotposre_i -o $plotposre_o_c
echo 16 | gmx energy -f $plotposre_i -o $plotposre_o_t

mdp="npt.mdp"
inp=$out
out="data/${proot}${shape}_${ff}_${wat}_npt"
gmx grompp -f $mdp -c $inp -p $topn -r $inp -o $out -t ${inp}.cpt

inp=$out
out=$inp
gmx mdrun -ntmpi $np -ntomp $ntomp -s $inp -deffnm $out
# We plot the pressure and the density to see the behaviour of the
# barostat
plotnpt_i="data/${proot}${shape}_${ff}_${wat}_npt.edr"
plotnpt_o_p="data/${proot}${shape}_${ff}_${wat}_npt_pres.xvg"
plotnpt_o_d="data/${proot}${shape}_${ff}_${wat}_npt_den.xvg"
echo 16 | gmx energy -f $plotnpt_i -o $plotnpt_o_p
echo 22 | gmx energy -f $plotnpt_i -o $plotnpt_o_d

mdp="pull.mdp"
inp=$out
out="data/${proot}${shape}_${ff}_${wat}_pull"
gmx grompp -f $mdp -c $inp -p $topn -r $inp -o $out -t ${inp}.cpt -n index.ndx -maxwarn 1

inp=$out
out=$inp
gmx mdrun -ntmpi $np -ntomp $ntomp -s $inp -deffnm $out

rm \#* data/\#*

outdir=RESULTS_${PBS_JOBID}
mkdir $PBS_O_WORKDIR/$outdir
mv -f $scrt/* $PBS_O_WORKDIR/$outdir
rmdir $scrt
