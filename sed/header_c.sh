#!/bin/bash

for file in *.c
do
  sed -i '1s/^/HaiCS (Hamiltonians in Computational Statistics)\n/' $file #this text contains 48 characters
  sed -i '2s/^/           Developed in the group of\n/' $file #the spaces at the beginning of each line are calculated as 48-the number of characters of the current text and divided by two
  sed -i '3s/^/        Professor Elena Akhmatskaya by:\n/' $file
  sed -i '4s/^/   Elena Akhmatskaya, Mario Fernandez-Pendas,\n/' $file
  sed -i '5s/^/          Felix Muller, Lorenzo Nagar,\n/' $file
  sed -i '6s/^/    Jorge Perez Heredia, Florian Puchhammer,\n/' $file
  sed -i '7s/^/               Tijana Radivojevic\n/' $file
  sed -i '8s/^/  For references to this package, please cite:\n/' $file
  sed -i '9s/^/    * Radivojevic, T. Enhancing Sampling in\n/' $file
  sed -i '10s/^/    Computational Statistics Using Modified\n/' $file
  sed -i '11s/^/    Hamiltonians. PhD Thesis, UPV\/EHU (2016)\n/' $file
  sed -i '12s/^/ * Radivojevic, T., Akhmatskaya, E. Modified\n/' $file
  sed -i '13s/^/Hamiltonian Monte Carlo for Bayesian inference.\n/' $file
  sed -i '14s/^/         Stat Comput 30, 377–404 (2020)\n/' $file
  sed -i '1s/./\/\/ &/' $file
  sed -i '2s/./\/\/ &/' $file
  sed -i '3s/./\/\/ &/' $file
  sed -i '4s/./\/\/ &/' $file
  sed -i '5s/./\/\/ &/' $file
  sed -i '6s/./\/\/ &/' $file
  sed -i '7s/./\/\/ &/' $file
  sed -i '8s/./\/\/ &/' $file
  sed -i '9s/./\/\/ &/' $file
  sed -i '10s/./\/\/ &/' $file
  sed -i '11s/./\/\/ &/' $file
  sed -i '12s/./\/\/ &/' $file
  sed -i '13s/./\/\/ &/' $file
  sed -i '14s/./\/\/ &/' $file
done
