#Mean and standard deviation of the densities
rho <- c(0.0707, 0.0651);
mu_r = mean(rho)
s_r = sd(rho)
print(mu_r)
print(s_r)

#Mean and standard deviation of the pressures
p <- c(0.05068816, 0.050200488);
mu_p = mean(p)
s_p = sd(p)
print(mu_p)
print(s_p)
