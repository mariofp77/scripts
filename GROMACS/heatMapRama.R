dfmd <- data.frame(x=rnorm(10000),y=rnorm(10000))
dfhmc <- data.frame(x=rnorm(10000),y=rnorm(10000))
dfgshmc <- data.frame(x=rnorm(10000),y=rnorm(10000))

dfmd <- read.table("~/Dropbox/BCAM/MultiStageIntegrators/MAIA/Results/NewApproach/Villin/HMC_MD_GSHMC/villin/ramacleanmd.txt")
dfhmc <- read.table("~/Dropbox/BCAM/MultiStageIntegrators/MAIA/Results/NewApproach/Villin/HMC_MD_GSHMC/villin/ramacleanhmc.txt")
dfgshmc <- read.table("~/Dropbox/BCAM/MultiStageIntegrators/MAIA/Results/NewApproach/Villin/HMC_MD_GSHMC/villin/ramacleangshmc.txt")

names(dfmd) <- c("x","y","label")
names(dfhmc) <- c("x","y","label")
names(dfgshmc) <- c("x","y","label")

#k1 <- with(dfmd[,1:2],MASS:::kde2d(x,y))
#filled.contour(k1)

#k2 <- with(dfhmc[,1:2],MASS:::kde2d(x,y))
#filled.contour(k2)

#k3 <- with(dfgshmc[,1:2],MASS:::kde2d(x,y))
#filled.contour(k3)

library(ggplot2)
#g <- ggplot(dfmd,aes(x=x,y=y))+geom_density2d() # Lines

# Better solution
g1 <- ggplot(dfmd,aes(x=x,y=y)) + stat_density2d(aes(x=x, y=y, fill = ..density..), geom="tile",contour = FALSE)
# The colors can be found in http://www.cookbook-r.com/Graphs/Colors_(ggplot2)/
#g1 <- g1 + scale_fill_gradientn(colours=c("#FFFFCC","#FFFF99","#FFFF66","#FFFF33","#FFFF00","#FFCC33","#FFCC00","#FF9900","#FF6600","#FF3300","#CC3300","#990000","#660000","#330000"),guide=FALSE)
g1 <- g1 + scale_fill_gradientn(colours=c("#999999", "#56B4E9","#0072B2", "#009E73", "#F0E442", "#E69F00", "#D55E00", "#CC79A7"),guide = FALSE)
g1 <- g1 + labs(x=expression(phi1),y=expression(psi)) # Labels
g1 <- g1 + scale_x_discrete(limits=c(-180,0,180),labels=c(expression(-pi),"0",expression(pi))) + scale_y_discrete(limits=c(-180,0,180),labels=c(expression(-pi),"0",expression(pi))) # Axis
g1 <- g1 + theme(axis.text=element_text(size=40,face="bold"),axis.title=element_text(size=40,face="bold")) # Size and style of the ticks and text in the axis
#g1 <- g1 + geom_vline(x=0) + geom_hline(y=0) # Vertical and horizontal lines
g1 <- g1 + geom_vline(xintercept=0) + geom_hline(yintercept=0) # Vertical and horizontal lines
g1 <- g1 + geom_point(colour="red",alpha=0.02) # Adds points
g1 <- g1 + theme(plot.margin=unit(c(1,1.5,1,1),"cm")) # size of the plot
print(g1)

g2 <- ggplot(dfhmc,aes(x=x,y=y)) + stat_density2d(aes(x=x, y=y, fill = ..density..), geom="tile",contour = FALSE)
# The colors can be found in http://www.cookbook-r.com/Graphs/Colors_(ggplot2)/
#g2 <- g2 + scale_fill_gradientn(colours=c("#FFFFCC","#FFFF99","#FFFF66","#FFFF33","#FFFF00","#FFCC33","#FFCC00","#FF9900","#FF6600","#FF3300","#CC3300","#990000","#660000","#330000"),guide=FALSE)
g2 <- g2 + scale_fill_gradientn(colours=c("#999999", "#56B4E9","#0072B2", "#009E73", "#F0E442", "#E69F00", "#D55E00", "#CC79A7"),guide = FALSE)
g2 <- g2 + labs(x=expression(phi1),y=expression(psi)) # Labels
g2 <- g2 + scale_x_discrete(limits=c(-180,0,180),labels=c(expression(-pi),"0",expression(pi))) + scale_y_discrete(limits=c(-180,0,180),labels=c(expression(-pi),"0",expression(pi))) # Axis
g2 <- g2 + theme(axis.text=element_text(size=40,face="bold"),axis.title=element_text(size=40,face="bold")) # Size and style of the ticks and text in the axis
#g2 <- g2 + geom_vline(x=0) + geom_hline(y=0) # Vertical and horizontal lines
g2 <- g2 + geom_vline(xintercept=0) + geom_hline(yintercept=0) # Vertical and horizontal lines
g2 <- g2 + geom_point(colour="red",alpha=0.02) # Adds points
g2 <- g2 + theme(plot.margin=unit(c(1,1.5,1,1),"cm")) # size of the plot
print(g2)

g3 <- ggplot(dfgshmc,aes(x=x,y=y)) + stat_density2d(aes(x=x, y=y, fill = ..density..), geom="tile",contour = FALSE)
# The colors can be found in http://www.cookbook-r.com/Graphs/Colors_(ggplot2)/
#g3 <- g3 + scale_fill_gradientn(colours=c("#FFFFCC","#FFFF99","#FFFF66","#FFFF33","#FFFF00","#FFCC33","#FFCC00","#FF9900","#FF6600","#FF3300","#CC3300","#990000","#660000","#330000"),guide=FALSE)
g3 <- g3 + scale_fill_gradientn(colours=c("#999999", "#56B4E9","#0072B2", "#009E73", "#F0E442", "#E69F00", "#D55E00", "#CC79A7"),guide = FALSE)
g3 <- g3 + labs(x=expression(phi1),y=expression(psi)) # Labels
g3 <- g3 + scale_x_discrete(limits=c(-180,0,180),labels=c(expression(-pi),"0",expression(pi))) + scale_y_discrete(limits=c(-180,0,180),labels=c(expression(-pi),"0",expression(pi))) # Axis
g3 <- g3 + theme(axis.text=element_text(size=40,face="bold"),axis.title=element_text(size=40,face="bold")) # Size and style of the ticks and text in the axis
#g3 <- g3 + geom_vline(x=0) + geom_hline(y=0) # Vertical and horizontal lines
g3 <- g3 + geom_vline(xintercept=0) + geom_hline(yintercept=0) # Vertical and horizontal lines
g3 <- g3 + geom_point(colour="red",alpha=0.02) # Adds points
g3 <- g3 + theme(plot.margin=unit(c(1,1.5,1,1),"cm")) # size of the plot
print(g3)