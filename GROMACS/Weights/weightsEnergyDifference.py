#!/usr/bin/python2

# Program by Mario Fernandez-Pendas
#
# This program, given a weights file as they are
# currently calculated in GROMACS rolls back their
# calculation and gives as an output a file with the
# differences between the real energies and the
# modified energies. In molecular simulation, these
# differences are usually negative.
# 
# It has to be remarked that the temperature is
# declared as T and it should be adapted for the
# simulation that is being analyzed.
#
# Usage: weightsEnergyDifference.py input output

import commands
import os
import sys
import math
import re

from optparse import OptionParser

# Some ANSI colors
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'

parser = OptionParser()
parser.add_option("-v","--verbose",dest="verbose",help="Print extra info",action="store_true")
(options,args) = parser.parse_args()

try:
	inputfile = file(args[0],"r") # read the first input file
	outputfile = file(args[1],"w") # write the second input file
except IOError:
	sys.stderr.write(FAIL)
	sys.stderr.write("There was a problem opening the file. Does it exist at all?")
	sys.stderr.write(ENDC+"\n")
	sys.exit(1)

data=0.0
w=0.0
k=0.008314462175
T=310.0
beta=1/(k*T)

fout=outputfile

if inputfile != None:
	filename = args[0]
	filelines = inputfile.readlines()

	for line in filelines:
		data = line.split()
		w = data[0]
		w = float(w)
		w = -math.log(w)/beta # roll back the transformation
		fout.write("%f \n" % w)
