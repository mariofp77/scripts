// Re-weight observables in the GSHMC simulation
#include <iostream> 
#include <fstream> 
#include <cstring> 
#include <cstdlib> 
using namespace std; 
 
int main(int argc, char *argv[]) 
{ 
  char buf1[256] = "";
  char buf2[256] = "";
  char sTemp[256] = "";
  double time = 0.0;
  double obsv = 0.0;
  double wght = 0.0;
  double ini=0.0, fin=0.0;
  double avg = 0.0, sum=0.0;
  int counter=0;
 
  if (argc < 4) 
  { 
    cout << "Usage: reweight <data_file> <weights_file> <output_file> <initial_time> <final_time>\n"; 
    return 1; 
  } 

  FILE *fData = fopen(argv[1], "r"); 
  if(!fData) { 
     cout << "Cannot open data file.\n"; 
     return 1; 
  } 
  FILE *fWeig = fopen(argv[2], "r"); 
  if(!fWeig) { 
     cout << "Cannot open weights file.\n"; 
     return 1; 
  } 
  FILE *fOutp = fopen(argv[3], "w"); 
  if(!fOutp) { 
     cout << "Cannot open output file.\n"; 
     return 1; 
  } 

  if (argc == 6)
  {
     ini = atof(argv[4]);
     fin = atof(argv[5]);
  }

  while (fgets(buf1, sizeof buf1, fData))
  {
     // read the header of the data file
     if ( !strncmp(buf1, "#", 1) || !strncmp(buf1, "@", 1) )
     {
        fprintf(fOutp, "%s", buf1);
     }
     // read the actual data and write weights
     else
     {
        sscanf(buf1, "%lf %lf", &time, &obsv);

        if (argc == 6)
        {
           if (time < ini)
              continue;
           if (time > fin)
              break;
        }
        else if (counter==0)
           ini = time;
        else
           fin = time;

        fgets(buf2, sizeof buf2, fWeig);
        wght = atof(buf2);
        if (wght == 0.0)
           continue;

        sum += wght;
        obsv *= wght;
        fprintf(fOutp, "%lf \t %lf \n", time, obsv);

        /* calculate the average of the observable */
        avg += obsv;
        counter++;
     }
  } 

  /* print the average of the observable */
  avg /= sum;
  fprintf(fOutp, "#Reweighted average = %lf in range %lf - %lf ps (%d steps). \n", avg, ini, fin, counter);
  printf(        " Reweighted average = %lf in range %lf - %lf ps (%d steps). \n", avg, ini, fin, counter);

  fclose(fData); 
  fclose(fWeig); 
  fclose(fOutp); 

  return 0; 
}

