#!/usr/bin/python2

# Program by Mario Fernandez-Pendas
#
# This program, given a weights file as they are
# currently calculated in GROMACS, normalizes them
# by their average value.
# 
# Usage: analyzeWeights.py input output

import commands
import os
import sys
import math
import re

from optparse import OptionParser

# Some ANSI colors
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'

parser = OptionParser()
parser.add_option("-v","--verbose",dest="verbose",help="Print extra info",action="store_true")
(options,args) = parser.parse_args()

try:
	inputfile = file(args[0],"r") # read the first input file
	outputfile = file(args[1],"w") # write the second input file
except IOError:
	sys.stderr.write(FAIL)
	sys.stderr.write("There was a problem opening the file. Does it exist at all?")
	sys.stderr.write(ENDC+"\n")
	sys.exit(1)

data=0.0
w=0.0
sum1=0.0
av=0.0
n=0.0

fout=outputfile

if inputfile != None:
	filename = args[0]
	filelines = inputfile.readlines()

	for line in filelines: # read all the weights and sum them
                n += 1.0
		data = line.split()
		w = data[0]
		w = float(w)
                sum1 += w
        av = sum1/n # calculate the average
        for line in filelines: # normalize all the weights by the average
		data = line.split()
		w = data[0]
		w = float(w)
                w = w/av
		fout.write("%f \n" % w)
