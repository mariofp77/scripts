#!/usr/bin/python2

# Program by Mario Fernandez-Pendas
#
# This program, given an output from an HMC/GHMC/GSHMC simulation,
# calculates the energy differences in the Metropolis tests
# of the positions, stores them in a .dat file and prints
# in the screen the average difference.
#
# The usual log files are not useful, since they do not have
# printed the energy differnces.
# The outputs from the queuing system, such as slurm
# (which are equivalent to what a virtual user would see in the
# screen during the simulation) are recommended.
# 
# Usage: metropolisEnergyDifferences.py input output

import commands
import os
import sys
import math
import re

from optparse import OptionParser

# Some ANSI colors
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'

parser = OptionParser()
parser.add_option("-v","--verbose",dest="verbose",help="Print extra info",action="store_true")
(options,args) = parser.parse_args()

try:
	inputfile = file(args[0],"r")
	outputfile = file(args[1],"w")
except IOError:
	sys.stderr.write(FAIL)
	sys.stderr.write("There was a problem opening the file. Does it exist at all?")
	sys.stderr.write(ENDC+"\n")
	sys.exit(1)

aux=0.0
ediff=0.0
average=0.0

fout=outputfile

if inputfile != None:
	filename = args[0]
	filelines = inputfile.readlines()

	re_diff = re.compile("dDelta_H")
        re_mdmc = re.compile("dDeltaXi = 0.000000") # This is for ensuring that only the differences in the MDMC step are considered

	i = 0
	for line in filelines:

		if (re_diff.search(line) and re_mdmc.search(line)):
		     aux = line.split('=')[2]
		     ediff = float(aux.split(',')[0])
                     fout.write("%5.8f \n" % ediff)
                     average = average+math.fabs(ediff)
		     i = i + 1
        average = average/i
        print("The average difference is %5.8f " % average)
