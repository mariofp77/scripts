#!/usr/bin/python2

# Program by Mario Fernandez-Pendas
#
# This program, given an output from a GSHMC simulation,
# calculates the differences between the proposed and the current
# true Hamiltonians in the Metropolis tests of the MDMC
# part. These differences are stored in a .dat file. 
#
# The usual log files are not useful, since they do not have
# printed the energy differnces.
# The outputs from the queuing system, such as slurm
# (which are equivalent to what a virtual user would see in the
# screen during the simulation) are recommended.
# 
# Usage: trueHamiltonianDifferences.py input output

import commands
import os
import sys
import math
import re

from optparse import OptionParser

# Some ANSI colors
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'

parser = OptionParser()
parser.add_option("-v","--verbose",dest="verbose",help="Print extra info",action="store_true")
(options,args) = parser.parse_args()

try:
	inputfile = file(args[0],"r")
	outputfile = file(args[1],"w")
except IOError:
	sys.stderr.write(FAIL)
	sys.stderr.write("There was a problem opening the file. Does it exist at all?")
	sys.stderr.write(ENDC+"\n")
	sys.exit(1)

shad=0.0
hami=0.0
ediff=0.0
average=0.0

fout=outputfile

if inputfile != None:
	filename = args[0]
	filelines = inputfile.readlines()

	re_hamcand = re.compile("TotalEnerCand")
	re_hamcurr = re.compile("TotalEnerCurr")
        re_mdmc = re.compile("dDeltaXi = 0.000000") # This ensures that only the differences in the MDMC step are considered

	i = 0
        j = 0
	for line in filelines:

		if re_hamcand.search(line):
                   cand = float(line.replace(', ',' ').split()[2])
                if re_hamcurr.search(line):
		   curr = float(line.replace(', ',' ').split()[5])
                if re_mdmc.search(line):
                   ediff = cand-curr
                   fout.write("%5.8f \n" % ediff)
                   j = j + 1 # The specific counter is used for the average, since only MDMC steps are used and i explores the whole file
                   average = average + math.fabs(ediff)
		i = i + 1
        average = average/j
        print("The average difference is %5.8f " % average)
