ESScoda <- function (xraw)
{
  NumOfSamples <- nrow(xraw)
  NumOfParameters <- ncol(xraw)
  x <- matrix(0, NumOfSamples, NumOfParameters)
  
  #find averages
  meanX <- colMeans(xraw)
  #standardize mean of samples 
  for (i in seq(1, NumOfParameters, 1)){
    x[,i] <- xraw[,i] - meanX[i]
  }
  
  ess <- rep(0, NumOfParameters)
  mcse <- rep(0, NumOfParameters)
  
  for (i in seq(1, NumOfParameters, 1)) {
    
    ess[i] <- effectiveSize(x[,i]);  
  
    #x0 <- (x[,i])*(x[,i]); 
    #var0 <- sum(x0)/(NumOfSamples-1);# variance
    #mcse0 <- sqrt(var0/ess[i]); # the same result
    
    var <- spectrum0.ar(x[,i])    
    
    mcse[i] <- sqrt(var$spec/NumOfSamples);
    
  }
  
  return(cbind(ess,mcse))
}