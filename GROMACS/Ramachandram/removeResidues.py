#!/usr/bin/python2

# Program by Mario Fernandez-Pendas
#
# This program, given an output from g_rama, removes the
# undesired residues (the list of key-words is included in
# bad_words[]). It also removes the header lines from
# GROMACS and those used by xmgrace.
# 
# Usage: removeResidues.py

bad_words = ['GLY','#','@']

with open('rama.xvg') as oldfile, open ('ramaclean.txt', 'w') as newfile:
    for line in oldfile:
        if not any(bad_word in line for bad_word in bad_words):
            newfile.write(line)
