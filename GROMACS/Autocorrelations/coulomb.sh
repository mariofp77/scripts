#!/bin/bash

## Extract the energy
echo $1 | g_energy_d_gshmc
cp energy.xvg coulomb.xvg

## Remove the header
sed -i -e "1,19d" coulomb.xvg

## Cut the part corresponding to the equilibration. The length is given as an input
cp coulomb.xvg coulomb_equil.xvg
sed -i -e "1,$2d" coulomb_equil.xvg

## Calculate the autocorrelation
g_analyze_d_gshmc -f coulomb_equil.xvg -ac ac_coulomb.xvg -gshmc -acflen $3
