#!/bin/bash

## Extract the energy
echo $1 | g_energy_d_gshmc
cp energy.xvg coulomb.xvg

## Remove the header
sed -i -e "1,19d" coulomb.xvg

## Save the points only in the Markov Chain. The frequency is given as an input
awk 'NR == 1 || NR % "'$2'" == 0' coulomb.xvg > coulomb_filtered.xvg

## Calculate the autocorrelation
g_analyze_d_gshmc -f coulomb_filtered.xvg -ac ac_coulomb_equil.xvg -gshmc -acflen $3
