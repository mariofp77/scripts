#!/bin/bash

## Extract the energy
echo $1 | g_energy_d_gshmc
cp energy.xvg coulomb.xvg

## Remove the header
sed -i -e "1,19d" coulomb.xvg

## Cut the part corresponding to the equilibration. The length is given as an input
cp coulomb.xvg coulomb_equil.xvg
sed -i -e "1,$2d" coulomb_equil.xvg

## Save the points only in the Markov Chain. The frequency is given as an input
awk 'NR == 1 || NR % "'$3'" == 0' coulomb_equil.xvg > coulomb_equil_filtered.xvg

## Calculate the autocorrelation
g_analyze_d_gshmc -f coulomb_equil_filtered.xvg -ac ac_coulomb_filtered.xvg -gshmc -acflen $4
