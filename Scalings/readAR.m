function AR=readAR
  % Load the acceptance rates. The text has to be removed from the file
  a=load('AR.txt');
  % Sotre the acceptance rates in a matrix 20x10 (number of time stepsxnumber of simulations)
  for i=1:20
    for j=1:10
      AR(i,j)=a(j+(i-1)*10);
    end
  end
 
  % Backup the matrix to reorder it since after the time step 1, the results for 10, 11, 12...
  % were stored
  Abackup=AR;
  % Move the time step 2 
  AR(2,:)=Abackup(12,:);
  % Move the time steps between 3 and 9 
  for i=3:9
    AR(i,:)=Abackup(i+11,:);
  end
  % Replace the time steps between 10 and 19
  for i=10:19
    AR(i,:)=Abackup(i-8,:);
  end
  % Replace the time step 20
  AR(20,:)=Abackup(13,:);
end
