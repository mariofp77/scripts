function average
  % Constants
  D = 167; w = 6.1896;
  % Time steps
  dt = linspace(0.01,0.04,20);

  % Call the function that reads the acceptance rates
  AR=readAR;
  
  % Main loop
  for i=1:length(dt)
    % Average acceptance rates and standard deviations
    meanAR(i) = mean(AR(i,:));
    stdAR(i) = std(AR(i,:));
    
    % Unique scalings using the mean acceptance rates
    Smean(i) = 1/(dt(i)*w)*(-32*log(meanAR(i))/D)^(1/6);
    
    % 10 scalings using the average acceptance rates of the 10 simulations which
    % have to be averaged afterwards. (This is what we have currently implemented)
    for j=1:10
      S(i,j) = 1/(dt(i)*w)*(-32*log(AR(i,j))/D)^(1/6);
    end
    meanofS(i) = mean(S(i,:));
    stdofS(i) = std(S(i,:));
  end
  
  Smean
  meanofS
  stdofS
end
